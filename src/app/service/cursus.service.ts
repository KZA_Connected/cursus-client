import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError as observableThrowError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { Cursus } from '../model/cursus';
import { Attitude } from '../model/attitude';
import { Functieniveau } from '../model/functieniveau';
import { Slagingscriterium } from '../model/slagingscriterium';
import { Status } from '../model/status';
import { EnvService  } from '../env.service';
import {Cursist} from '../model/cursist';

@Injectable()
export class CursusService {
  private cursussenUrl = '/cursussen';
  private attitudesUrl = '/attitudes';
  private functieniveausUrl = '/functieniveaus';
  private slagingscriteriaUrl = '/slagingscriteria';
  private statussenUrl = '/statussen';
  private apiUrl = '';

  constructor(
    private http: HttpClient,
    private env: EnvService
  ) {this.apiUrl = env.apiUrl}

  getCursussen() {
    return this.http
      .get<Cursus[]>(this.apiUrl + this.cursussenUrl)
      .pipe(map(data => data), catchError(this.handleError));
  }

  getCursus(id: number) {
    const url = `${this.cursussenUrl}/${id}`;
    return this.http
      .get<Cursus>(this.apiUrl + url)
      .pipe(map(data => data), catchError(this.handleError));
  }

  getAttitudes() {
    return this.http
      .get<Attitude[]>(this.apiUrl + this.attitudesUrl)
      .pipe(map(data => data), catchError(this.handleError));
  }

  getFunctieniveaus() {
    return this.http
      .get<Functieniveau[]>(this.apiUrl + this.functieniveausUrl)
      .pipe(map(data => data), catchError(this.handleError));
  }

  getSlagingscriteria() {
    return this.http
      .get<Slagingscriterium[]>(this.apiUrl + this.slagingscriteriaUrl)
      .pipe(map(data => data), catchError(this.handleError));
  }

  getStatussen() {
    return this.http
      .get<Status[]>(this.apiUrl + this.statussenUrl)
      .pipe(map(data => data), catchError(this.handleError));
  }

  saveCursus(cursus: Cursus) {
    if (cursus.id) {
      return this.put(cursus);
    }
    return this.post(cursus);
  }

  deleteCursus(cursus: Cursus) {
    const url = `${this.cursussenUrl}/${cursus.id}`;
    return this.http
      .delete<Cursus>(this.apiUrl + url)
      .pipe(catchError(this.handleError));
  }

  private post(cursus: Cursus) {
    return this.http
      .post<Cursus>(this.apiUrl + this.cursussenUrl, cursus)
      .pipe(catchError(this.handleError));
  }

  postCursist(cursusId: Number, cursist: Cursist) {
    const url = `${this.cursussenUrl}/${cursusId}/cursuscursisten`;
    return this.http
      .post<Cursus>(this.apiUrl + url, cursist)
      .pipe(catchError(this.handleError));
  }

  private put(cursus: Cursus) {
    const url = `${this.cursussenUrl}/${cursus.id}`;
    return this.http
      .put<Cursus>(this.apiUrl + url, cursus)
      .pipe(catchError(this.handleError));
  }

  private handleError(res: HttpErrorResponse | any) {
    console.error(res.error || res.body.error);
    return observableThrowError(res.error || 'Server error');
  }
}
