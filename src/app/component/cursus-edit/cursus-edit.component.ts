import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';

import { Cursus } from '../../model/cursus';
import { Attitude } from '../../model/attitude';
import { Functieniveau } from '../../model/functieniveau';
import { Slagingscriterium } from '../../model/slagingscriterium';
import { Status } from '../../model/status';
import { CursusService } from '../../service/cursus.service';

@Component({
  selector: 'my-cursus-edit',
  templateUrl: './cursus-edit.component.html',
  styleUrls: ['./cursus-edit.component.css']
})
export class CursusEditComponent implements OnInit {
  cursus: Cursus;
  attitudes: Attitude[];
  functieniveaus: Functieniveau[];
  slagingscriteria: Slagingscriterium[];
  statussen: Status[];

  constructor(
    private cursusService: CursusService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getCursus();
    this.getAttitudes();
    this.getFunctieniveaus();
    this.getSlagingscriteria();
    this.getStatussen();
  }

  getCursus(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.cursusService
      .getCursus(id)
      .subscribe(
        cursus => {this.cursus = cursus},
        error => {this.router.navigate([''])}
      );
  }

  getAttitudes(): void {
    this.cursusService
      .getAttitudes()
      .subscribe(
        attitudes => {this.attitudes = attitudes}
      );
  }

  getFunctieniveaus(): void {
    this.cursusService
      .getFunctieniveaus()
      .subscribe(
        functieniveaus => {this.functieniveaus = functieniveaus}
      );
  }

  getSlagingscriteria(): void {
    this.cursusService
      .getSlagingscriteria()
      .subscribe(
        slagingscriteria => {this.slagingscriteria = slagingscriteria}
      );
  }

  getStatussen(): void {
    this.cursusService
      .getStatussen()
      .subscribe(
        statussen => {this.statussen = statussen}
      );
  }

  addDocent(cursus: Cursus): void {
    cursus.cursusdocenten.push({
      id: null,
      naam: '',
      email: ''
    });
  }

  removeDocent(cursus: Cursus, index: number): void {
    cursus.cursusdocenten.splice(index, 1);
  }

  removeCursist(cursus: Cursus, index: number): void {
    cursus.cursuscursisten.splice(index, 1);
  }

  addDatum(cursus: Cursus): void {
    cursus.cursusdata.push({
      id: null,
      datum: ''
    });
  }

  removeDatum(cursus: Cursus, index: number): void {
    cursus.cursusdata.splice(index, 1);
  }

  setDefaultDatum(cursus: Cursus, index: number): void {
    const date = new Date();
    const string = '01-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + date.getFullYear() + ' 18:00:00';
    cursus.cursusdata.splice(index, 1, {id: null, datum: string});
  }

  save(): void {
    if (this.cursusHasDataEnDocenten()) {
      this.cursusService
        .saveCursus(this.cursus)
        .subscribe(
          cursus => {
            this.cursus = cursus;
            this.router.navigate(['/detail', cursus.id]);
          }
        );
    }
  }

  cursusHasDataEnDocenten(): boolean {
    let hasData = true;
    const pattern = new RegExp('^(\\d{2})-(\\d{2})-(\\d{4}) (\\d{2}):(\\d{2}):(\\d{2})$');
    this.cursus.cursusdata.forEach(
      cursusDatum => { if (!pattern.test(cursusDatum.datum)) { hasData = false; }}
    );
    this.cursus.cursusdocenten.forEach(
      cursusDocent => { if (cursusDocent.naam === '' || cursusDocent.email === '') { hasData = false; }}
    );
    return hasData;
  }

  goBack(): void {
    window.history.back();
  }
}
