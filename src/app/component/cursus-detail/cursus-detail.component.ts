import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";

import { CursusService } from "../../service/cursus.service";
import { Cursus } from "../../model/cursus";
import { Cursist } from "../../model/cursist";
import { AdalService } from "adal-angular4";

@Component({
  selector: "my-cursus-detail",
  templateUrl: "./cursus-detail.component.html",
  styleUrls: ["./cursus-detail.component.css"]
})
export class CursusDetailComponent implements OnInit {
  cursus: Cursus;
  naam: string;
  email: string;
  isAangemeld: boolean;
  isPreviousDate: boolean = false;

  constructor(
    private adalService: AdalService,
    private cursusService: CursusService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getCursus();
    this.naam = this.adalService.userInfo.profile.name;
    this.email = this.adalService.userInfo.userName;
  }

  getIsAangemeld(): void {
    this.isAangemeld = false;
    this.cursus.cursuscursisten.forEach(cursist => {
      if (cursist.email === this.email) {
        this.isAangemeld = true;
      }
    });
  }

  getCursus(): void {
    const id = +this.route.snapshot.paramMap.get("id");
    this.cursusService.getCursus(id).subscribe(
      cursus => {
        this.cursus = cursus;
        this.getIsAangemeld();
        this.checkIfPreviousDate();
      },
      error => {
        this.router.navigate([""]);
      }
    );
  }

  apply(event: any): void {
    event.stopPropagation();
    const cursist = new Cursist();
    cursist.naam = this.naam;
    cursist.email = this.email;
    this.cursusService.postCursist(this.cursus.id, cursist).subscribe();
    this.isAangemeld = true;
    this.cursus.cursuscursisten.push({
      id: null,
      naam: this.naam,
      email: this.email
    });
  }

  goBack(): void {
    this.router.navigate(["/cursussen"]);
  }

  checkIfPreviousDate(): void {
    // get the last date of cursusdata.datum
    // the data object is in US, so split it and transfer it to EU standards
    // compare last date with current date
    // return true if the data is previous
    // add condition html, if checkpreviousdata is true, disable the button

    let prev = this.cursus.cursusdata[0].datum.split("-", 10);

    let currentDate = new Date();
    let previousDate = new Date(`${prev[1]}-${prev[0]}-${prev[2]}`);

    if (currentDate > previousDate) {
      this.isPreviousDate = true;
    }
  }
}
