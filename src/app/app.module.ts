import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpInterceptorService } from './service/httpInterceptor.service';

import { AppRoutingModule } from './app-routing.module';
import { CursusService } from './service/cursus.service';

import { SuiModule } from 'ng2-semantic-ui';
import { AdalService } from 'adal-angular4';

import { AppComponent } from './component/app.component';
import { CursusListComponent } from './component/cursus-list/cursus-list.component';
import { CursusDetailComponent } from './component/cursus-detail/cursus-detail.component';
import { CursusNewComponent } from './component/cursus-new/cursus-new.component';
import { CursusEditComponent } from './component/cursus-edit/cursus-edit.component';
import { EnvServiceProvider } from './env.service.provider';

@NgModule({
  imports: [
    SuiModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  declarations: [
    AppComponent,
    CursusListComponent,
    CursusDetailComponent,
    CursusNewComponent,
    CursusEditComponent
  ],
  providers: [
    CursusService,
    EnvServiceProvider,
    AdalService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
